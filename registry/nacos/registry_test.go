package nacos

import (
	"context"
	"gitee.com/llakcs/agile-go/registry"
	"testing"
	"time"
)

func TestServer(t *testing.T) {
	service2 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8087?isSecure=false"},
	}
	service3 := &registry.ServiceInstance{
		ID:        "1234sdaweqewrt",
		Name:      "test2",
		Version:   "1.0",
		Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8086?isSecure=false"},
	}
	confs := make([]ServerConfig, 0)
	conf := ServerConfig{
		IpAddr: "127.0.0.1",
		Port:   18848,
	}
	confs = append(confs, conf)
	r, err := NewRegistry(
		WithUserName("nacos"),
		WithPassWord("nacos"),
		WithWeight(10),
		WithNameSpaceId("test"),
		WithServerConfs(confs),
		WithNotLoadCacheAtStart(true),
	)
	r2, err2 := NewRegistry(
		WithUserName("nacos"),
		WithPassWord("nacos"),
		WithWeight(2),
		WithNameSpaceId("test"),
		WithServerConfs(confs),
		WithNotLoadCacheAtStart(true),
	)
	if err2 != nil {
		t.Fatal(err2)
	}
	if err != nil {
		t.Fatal(err)
	}
	e := r.Register(context.Background(), service2)
	if e != nil {
		t.Log("server fail:", e)
	}
	time.Sleep(1 * time.Second)
	e2 := r2.Register(context.Background(), service3)
	if e2 != nil {
		t.Log("server2 fail", e2)
	}
	//time.Sleep(30 * time.Second)
	//r.Deregister(context.Background(), service2)
	//t.Log("dereg")
	select {}
}

func TestNacosReg(t *testing.T) {
	service := &registry.ServiceInstance{
		ID:      "123aaabbbccddd",
		Name:    "agile-nacos-test-service",
		Version: "1.0",
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"http://127.0.0.1:8080?isSecure=false"},
	}
	service2 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8088?isSecure=false"},
	}

	confs := make([]ServerConfig, 0)
	conf := ServerConfig{
		IpAddr: "127.0.0.1",
		Port:   18848,
	}
	confs = append(confs, conf)
	r, err := NewRegistry(
		WithUserName("nacos"),
		WithPassWord("nacos"),
		WithWeight(100),
		WithNameSpaceId("test"),
		WithServerConfs(confs),
		WithNotLoadCacheAtStart(true),
	)
	if err != nil {
		t.Fatal(err)
	}

	e1 := r.Register(context.Background(), service)
	if e1 != nil {
		t.Log("server fail:", e1)
	}
	time.Sleep(1 * time.Second)
	serverinstants, gerr := r.GetService(context.Background(), service2.Name)
	if gerr != nil {
		t.Log("获取服务错误:", gerr)
	}
	for _, serverInfo := range serverinstants {
		t.Log("info:", serverInfo)
	}
	w, e2 := r.Watch(context.Background(), service2.Name)
	if e2 != nil {
		t.Log("watcher err:", e2)
	}
	//go func() {
	//
	//e := r.namingClient.Subscribe(&vo.SubscribeParam{
	//	ServiceName: service2.Name,
	//	Clusters:    []string{"DEFAULT"},
	//	GroupName:   "DEFAULT_GROUP",
	//	SubscribeCallback: func(services []model.Instance, err error) {
	//		fmt.Println("watcher---------------------------------------!")
	//	},
	//})
	//if e != nil {
	//	t.Log("watcher err:", e)
	//}

	//}()

	w.Next()
	select {}
}
