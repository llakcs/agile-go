package json

import (
	"testing"
)

func TestJsonCodec(t *testing.T) {
	// JSON 数据
	jsonData := []byte(`{"message": "Hello, World!"}`)

	// 创建 Result 类型的变量
	var result struct {
		Message string `json:"message"`
	}

	// 创建 JsonCodec 实例
	codec := JsonCodec{}

	// 使用自定义的 Unmarshal 方法解码 JSON 数据
	err := codec.Unmarshal(jsonData, &result)
	if err != nil {
		t.Log("解码失败:", err)
		return
	}

	// 输出解码后的结果
	t.Log(result.Message)
}
