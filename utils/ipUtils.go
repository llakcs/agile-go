package utils

import (
	"gitee.com/llakcs/agile-go/log"
	"net"
)

// 获取本地IP
func GetLocalIpAddr() string {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		log.Log(log.LevelError, "net.Interfaces failed, err: %v", err)
		return ""
	}
	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()

			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						//log.Println(ipnet.IP.String())
						return ipnet.IP.String()
					}
				}
			}
		}
	}
	return ""
}
