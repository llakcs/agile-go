package grpc

import (
	"context"
	"fmt"
	"gitee.com/llakcs/agile-go/transport/testdata/pb"
	"io"
	"strconv"
	"testing"
	"time"
)

type userServer struct {
	pb.UnimplementedUserInfoServiceServer
}

type demoServer struct {
	pb.UnimplementedDemoServiceServer
}

type chatServer struct {
	pb.UnimplementedChatServer
}

// 双向流式
func (c *chatServer) SendMessage(stream pb.Chat_SendMessageServer) error {
	for {
		req, err := stream.Recv()
		if err == io.EOF { //接收完了
			return nil
		}
		if err != nil {
			return err
		}
		fmt.Println("##接收到的信息:", req.Msg)
		sendErr := stream.Send(&pb.MessageResponse{Msg: "hello world--------!"})
		if err != nil {
			fmt.Println("发送错误:", sendErr)
		}
	}
}

// 普通rpc
func (d *demoServer) GetProjectInfo(ctx context.Context, ao *pb.ProjectAo) (*pb.ProjectVo, error) {
	return &pb.ProjectVo{
		Users:       nil,
		Devices:     nil,
		ProjectName: "adb",
		ProjectId:   "123",
		Admins:      nil,
	}, nil
}

// 服务器流式
func (d *demoServer) GetLogData(ao *pb.LogAo, stream pb.DemoService_GetLogDataServer) error {
	fmt.Println("##服务器流失入参:", ao.ReqId)
	for i := 0; i < 10; i++ {
		err := stream.Send(&pb.LogVo{
			Level:     "info",
			Timestamp: strconv.Itoa(i),
			Msg:       "cccdddddddddd",
		})
		if err != nil {
			return err
		}
		time.Sleep(500 * time.Millisecond)
	}
	return nil
}

// 客户端流式
func (d *demoServer) UploadData(stream pb.DemoService_UploadDataServer) error {
	for {
		req, err := stream.Recv()
		if req != nil {
			fmt.Println(req.Data)
		}
		if err == io.EOF {
			return stream.SendAndClose(&pb.UploadDataVo{
				Code: 0,
				Msg:  "success",
			})
		}
		if err != nil {
			return err
		}
	}
}

func (s *userServer) GetUserInfo(ctx context.Context, req *pb.UserRequest) (*pb.UserResponse, error) {
	fmt.Println("###GetUserInfo-----!")
	strs := make([]string, 0)
	strs = append(strs, "hello")
	strs = append(strs, "world")
	return &pb.UserResponse{
		Id:    1,
		Name:  "lee",
		Age:   25,
		Title: strs,
	}, nil
}

func TestGrpcServer1(t *testing.T) {
	s := NewServer(Address(":8086"))
	pb.RegisterChatServer(s, &chatServer{})
	pb.RegisterDemoServiceServer(s, &demoServer{})
	pb.RegisterUserInfoServiceServer(s, &userServer{})
	if err := s.Start(context.Background()); err != nil {
		panic(err)
	}
}

func TestGrpcServer2(t *testing.T) {
	s := NewServer(Address(":8087"))
	pb.RegisterChatServer(s, &chatServer{})
	pb.RegisterDemoServiceServer(s, &demoServer{})
	pb.RegisterUserInfoServiceServer(s, &userServer{})
	if err := s.Start(context.Background()); err != nil {
		panic(err)
	}
}
