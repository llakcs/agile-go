package grpc

import (
	"context"
	"fmt"
	"gitee.com/llakcs/agile-go/registry"
	"gitee.com/llakcs/agile-go/registry/nacos"
	"gitee.com/llakcs/agile-go/selector"
	"gitee.com/llakcs/agile-go/transport/testdata/pb"

	"google.golang.org/grpc/balancer"
	"google.golang.org/grpc/balancer/base"
	"log"
	"strconv"
	"testing"
)

// 测试gprc服务发现
func TestDialForDiscovery(t *testing.T) {
	b := base.NewBalancerBuilder(
		balancerName,
		&balancerBuilder{
			builder: selector.GlobalSelector(),
		},
		base.Config{HealthCheck: true},
	)
	balancer.Register(b)
	service := &registry.ServiceInstance{
		ID:      "aaa",
		Name:    "discovery-servicce",
		Version: "1.0",
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8088?isSecure=false"},
	}
	confs := make([]nacos.ServerConfig, 0)
	conf := nacos.ServerConfig{
		IpAddr: "127.0.0.1",
		Port:   18848,
	}
	confs = append(confs, conf)
	r, err := nacos.NewRegistry(
		nacos.WithUserName("nacos"),
		nacos.WithPassWord("nacos"),
		nacos.WithWeight(100),
		nacos.WithNameSpaceId("test"),
		nacos.WithServerConfs(confs),
		nacos.WithNotLoadCacheAtStart(true),
	)
	if err != nil {
		t.Fatal(err)
	}
	e := r.Register(context.Background(), service)
	if e != nil {
		t.Log("server fail:", e)
		return
	}

	conn, derr := DialInsecure(context.Background(), WithDiscovery(r), WithEndpoint("discovery:///test2"))
	if derr != nil {
		t.Fatal(derr)
	}
	defer conn.Close()
	user := pb.NewUserInfoServiceClient(conn)
	for i := 0; i < 30; i++ {
		res, gerr := user.GetUserInfo(context.Background(), &pb.UserRequest{Name: "lee"})
		if gerr != nil {
			t.Log(gerr)
		}
		t.Log("res:{}", res)
	}

}

func TestDial(t *testing.T) {

	conn, connerr := DialInsecure(context.Background(), WithEndpoint(":8080"))
	if connerr != nil {
		t.Fatal(connerr)
	}
	defer conn.Close()
	// 创建双向流客户端
	chatClient := pb.NewChatClient(conn)
	demoClient := pb.NewDemoServiceClient(conn)

	//服务器流式
	listStream, listErr := demoClient.GetLogData(context.Background(), &pb.LogAo{ReqId: "1"})
	if listErr != nil {
		fmt.Println("listerr:", listErr)
	}
	for {
		resp, err := listStream.Recv()
		if err != nil {
			log.Println("接收服务器流式响应失败：", err)
		}
		if resp != nil {
			fmt.Println("msg:", resp.Msg, "level:", resp.Level, "time:", resp.Timestamp)
		}
		if resp.Timestamp == "9" {
			break
		}
	}

	//客户端流失
	fmt.Println("###客户端流式")
	recordStream, rerr := demoClient.UploadData(context.Background())
	if rerr != nil {
		log.Println("调用 Record 函数失败：", rerr)
	}
	str := "Hello, World!"
	for i := 0; i < 5; i++ {
		a := str + strconv.Itoa(i)
		bs := []byte(a)
		err := recordStream.Send(&pb.UploadDataAo{Data: bs})
		if err != nil {
			log.Println("发送客户端流式请求失败：", err)
		}
	}
	fmt.Println("###客户端流式22222222222")
	resp, err := recordStream.CloseAndRecv()
	if err != nil {
		log.Println("接收客户端流式响应失败：", err)
	}
	fmt.Println("resp:", resp.Code, resp.Msg)

	//双向流式
	routeStream, err := chatClient.SendMessage(context.Background())
	if err != nil {
		log.Println("调用 Route 函数失败：", err)
	}

	for i := 0; i < 5; i++ {
		err := routeStream.Send(&pb.MessageRequest{Msg: "你好"})
		if err != nil {
			log.Println("发送双向流式请求失败：", err)
		}
		resp, err := routeStream.Recv()
		if err != nil {
			log.Println("接收双向流式响应失败：", err)
		}
		fmt.Println(resp.Msg)
	}

	routeStream.CloseSend()
	t.Log("finsh")
}
