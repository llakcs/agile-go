package target

import (
	"net/url"
	"strings"
)

type Target struct {
	Scheme    string
	Authority string
	Endpoint  string
}

func ParseTarget(endpoint string, insecure, isGrpc bool) (*Target, error) {
	if !isGrpc {
		if !strings.Contains(endpoint, "://") {
			if insecure {
				endpoint = "http://" + endpoint
			} else {
				endpoint = "https://" + endpoint
			}
		}
	}
	u, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}
	target := &Target{Scheme: u.Scheme, Authority: u.Host}
	if len(u.Path) > 1 {
		target.Endpoint = u.Path[1:]
	}
	return target, nil
}
