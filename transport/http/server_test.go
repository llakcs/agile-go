package http

import (
	"context"
	"github.com/gin-gonic/gin"
	"testing"
)

func TestHttpServer(t *testing.T) {
	r := gin.Default()
	r.GET("/testGet", func(c *gin.Context) {
		// 获取URL查询参数
		name := c.Query("name")
		age := c.Query("age")
		c.JSON(200, gin.H{
			"name": name,
			"age":  age,
		})
		t.Log("name:", name, "age:", age)
	})
	NewServer(WithRouter(r)).Start(context.Background())
}
