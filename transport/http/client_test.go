package http

import (
	"gitee.com/llakcs/agile-go/encoding"
	jsonAgile "gitee.com/llakcs/agile-go/encoding/json"
	"gitee.com/llakcs/agile-go/middleware"
	"gitee.com/llakcs/agile-go/registry"
	"gitee.com/llakcs/agile-go/registry/nacos"
	"gitee.com/llakcs/agile-go/selector"

	"context"
	"crypto/tls"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"testing"
	"time"
)

type Result struct {
	Msg  string `json:"msg"`
	Code string `json:"code"`
	Time int64  `json:"time"`
}

type BaseResult struct {
	Code string
	Msg  string
	Time int64
	Data interface{}
}

type RequestBody struct {
	SN        string `json:"sn"`
	ProjectId string `json:"projectId"`
	command   string `json:"command"`
}

func TestHttpTlsClient(t *testing.T) {
	encoding.RegisterCodec(jsonAgile.JsonCodec{}) //注册解码器
	c, err := NewClient(context.Background(),
		WithEndpoint("192.168.3.190:21117"),
		WithTLSConfig(&tls.Config{InsecureSkipVerify: true}), //tls
		WithMiddleware(func(handler middleware.Handler) middleware.Handler {
			t.Logf("handle in middleware")
			return func(ctx context.Context, req interface{}) (interface{}, error) {
				return handler(ctx, req)
			}
		}))
	if err != nil {
		t.Fatal(err)
	}
	resp := &Result{}
	reqErr := c.Invoke(context.Background(), http.MethodPost, MediaTypeJSON, "/api/hardware/environment/monitor/getAreaDailyAverage", map[string]string{"pageNum": "10"}, resp)
	if reqErr != nil {
		t.Error(reqErr)
	}
	t.Log("###返回", resp.Code, "msg:", resp.Msg, "time:", resp.Time)
}

func TestHttpClientWithDecodeResponseFunc(t *testing.T) {
	encoding.RegisterCodec(jsonAgile.JsonCodec{}) //注册解码器
	// 组装请求体
	requestBody := RequestBody{
		SN:        "123456",
		ProjectId: "12",
		command:   "888",
	}
	r := &BaseResult{}
	c, err := NewClient(context.Background(), WithEndpoint("127.0.0.1:8086"), WithDecodeResponseFunc(func(ctx context.Context, res *http.Response, out interface{}) error {
		detail := ""
		data, err := io.ReadAll(res.Body)
		if err == nil {
			detail = string(data)
		}
		t.Log("detail", detail)
		//开始转化
		json.Unmarshal(data, r)
		out = r
		return nil
	}))
	if err != nil {
		t.Fatal(err)
	}

	reqErr := c.Invoke(context.Background(), http.MethodPost, MediaTypeJSON, "/deviceReqLog/query/pageList", requestBody, r)
	if reqErr != nil {
		t.Fatal(reqErr)
	}
	t.Log("返回值 code:", r.Code, "msg:", r.Msg, "time:", r.Time, "data:", r.Data)
}

func TestServer(t *testing.T) {
	g := gin.Default()
	g.GET("/test_discovery", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"code": "0",
			"msg":  "success",
			"time": 123344565,
			"data": "love",
		})
	})
	g.Run(":8833")
}

func TestWithDiscovery(t *testing.T) {
	encoding.RegisterCodec(jsonAgile.JsonCodec{})                       //注册解码器
	selector.SetGlobalSelector(selector.NewWeightedRoundRobinBuilder()) //
	service := &registry.ServiceInstance{
		ID:      "aaa",
		Name:    "discovery-servicce",
		Version: "1.0",
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8088?isSecure=false"},
	}
	confs := make([]nacos.ServerConfig, 0)
	conf := nacos.ServerConfig{
		IpAddr: "127.0.0.1",
		Port:   18848,
	}
	confs = append(confs, conf)
	r, err := nacos.NewRegistry(
		nacos.WithUserName("nacos"),
		nacos.WithPassWord("nacos"),
		nacos.WithWeight(100),
		nacos.WithNameSpaceId("test"),
		nacos.WithServerConfs(confs),
		nacos.WithNotLoadCacheAtStart(true),
	)
	if err != nil {
		t.Fatal(err)
	}
	e := r.Register(context.Background(), service)
	if e != nil {
		t.Log("server fail:", e)
		return
	}

	// 组装请求体
	//requestBody := RequestBody{
	//	SN:        "123456",
	//	ProjectId: "12",
	//	command:   "888",
	//}
	br := &BaseResult{}
	c, cerr := NewClient(context.Background(), WithDiscovery(r), WithEndpoint("discovery:///test2"))
	if cerr != nil {
		t.Fatal(cerr)
	}
	for i := 0; i < 10; i++ {
		reqErr := c.Invoke(context.Background(), http.MethodGet, MediaTypeJSON, "/test_discovery", nil, br)
		if reqErr != nil {
			t.Log(reqErr)
		}
		time.Sleep(1 * time.Second)
	}

}

func TestHttpClient(t *testing.T) {
	encoding.RegisterCodec(jsonAgile.JsonCodec{}) //注册解码器
	// 组装请求体
	requestBody := RequestBody{
		SN:        "123456",
		ProjectId: "12",
		command:   "888",
	}
	r := &BaseResult{}
	c, err := NewClient(context.Background(), WithEndpoint("127.0.0.1:8086"))
	if err != nil {
		t.Fatal(err)
	}
	reqErr := c.Invoke(context.Background(), http.MethodPost, MediaTypeJSON, "/deviceReqLog/query/pageList", requestBody, r)
	if reqErr != nil {
		t.Fatal(reqErr)
	}
	t.Log("返回值 code:", r.Code, "msg:", r.Msg, "time:", r.Time, "data:", r.Data)
}
