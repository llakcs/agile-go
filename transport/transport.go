package transport

import (
	"context"
	"net/url"
)

// Server is transport server.
type Server interface {
	Start(context.Context) error
	Stop(context.Context) error
}

// Endpointer is registry endpoint.
type Endpointer interface {
	Endpoint() (*url.URL, error)
}

// Header is the storage medium used by a Header.
type Header interface {
	Get(key string) string
	Set(key string, value string)
	Add(key string, value string)
	Keys() []string
	Values(key string) []string
}

const (
	KindGRPC Kind = "grpc"
	KindHTTP Kind = "http"
)

type Kind string

func (k Kind) String() string { return string(k) }
