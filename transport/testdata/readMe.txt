
//单个被引用的类使用go_out就行
命令: protoc --go_out=./protobuf/pb/ ./protobuf/proto/manger.proto

//最后一个路径是proto文件所在位置
protoc --go_out=./protobuf/pb/ --go-grpc_out=./protobuf/pb/ ./protobuf/proto/*.proto

//有引用的类 import  --proto_path={proto对应的路径}   out是对应代码生成的路径  最后一个路径是proto文件所在位置
 protoc --proto_path=./protobuf/proto/ --go_out=./protobuf/pb/ --go-grpc_out=./protobuf/pb/ ./protobuf/proto/*.proto




