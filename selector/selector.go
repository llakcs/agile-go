package selector

import "context"

type Node interface {

	// Address is the unique address under the same service
	Address() string

	// ServiceName is service name
	ServiceName() string

	// InitialWeight is the initial value of scheduling weight
	// if not set return nil
	InitialWeight() int64

	// Version is service node version
	Version() string

	// Metadata is the kv pair metadata associated with the service instance.
	// version,namespace,region,protocol etc..
	Metadata() map[string]string
}

type Selector interface {
	Add(nodes ...Node) error
	Next(ctx context.Context) (selected Node, err error)
}

type Builder interface {
	Build() Selector
}

var globalSelector Builder

func GlobalSelector() Builder {
	return globalSelector
}

func SetGlobalSelector(b Builder) {
	globalSelector = b
}

type DoneInfo struct {
	// Response Error
	Err error
	// Response Metadata
	ReplyMD ReplyMD

	// BytesSent indicates if any bytes have been sent to the server.
	BytesSent bool
	// BytesReceived indicates if any byte has been received from the server.
	BytesReceived bool
}

// ReplyMD is Reply Metadata.
type ReplyMD interface {
	Get(key string) string
}

// DoneFunc is callback function when RPC invoke done.
type DoneFunc func(ctx context.Context, di DoneInfo)
