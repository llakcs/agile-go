package selector

import (
	"context"
	"gitee.com/llakcs/agile-go/registry"
	"testing"
)

// 测试加权随机
func TestWeight_Random(t *testing.T) {
	wr := NewWeightRandomBanlance()
	service := &registry.ServiceInstance{
		ID:      "123aaabbbccddd",
		Name:    "agile-nacos-test-service",
		Version: "1.0",
		Weight:  3,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"http://127.0.0.1:8080?isSecure=false"},
	}
	service2 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		Weight:  2,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8086?isSecure=false"},
	}
	n1 := NewNode("127.0.0.1:1111", service)
	n2 := NewNode("127.0.0.2:2222", service2)
	wr.Add(n1, n2)
	//获取节点
	for i := 0; i < 30; i++ {
		n, err := wr.Next(context.Background())
		if err != nil {
			t.Log(err)
		}
		t.Log("##node addr:", n.Address(), "weight:", n.InitialWeight())
	}
}

// 测试加权轮询
func TestWeight_Round_Robin(t *testing.T) {
	wrr := NewWeightedRoundRobin()
	service := &registry.ServiceInstance{
		ID:      "123aaabbbccddd",
		Name:    "agile-nacos-test-service",
		Version: "1.0",
		Weight:  3,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"http://127.0.0.1:8080?isSecure=false"},
	}
	service2 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		Weight:  2,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8086?isSecure=false"},
	}
	n1 := NewNode("127.0.0.1:1111", service)
	n2 := NewNode("127.0.0.2:2222", service2)
	wrr.Add(n1, n2)
	for i := 0; i < 30; i++ {
		n, err := wrr.Next(context.Background())
		if err != nil {
			t.Log(err)
		}
		t.Log("##node addr:", n.Address(), "weight:", n.InitialWeight())
	}
}

// 测试随机
func TestRandomBanlance(t *testing.T) {
	rd := NewRandomBanlance()
	service := &registry.ServiceInstance{
		ID:      "123aaabbbccddd",
		Name:    "agile-nacos-test-service",
		Version: "1.0",
		Weight:  3,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"http://127.0.0.1:8080?isSecure=false"},
	}
	service2 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		Weight:  2,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8086?isSecure=false"},
	}
	n1 := NewNode("127.0.0.1:1111", service)
	n2 := NewNode("127.0.0.2:2222", service2)
	rd.Add(n1, n2)
	for i := 0; i < 30; i++ {
		n, err := rd.Next(context.Background())
		if err != nil {
			t.Log(err)
		}
		t.Log("##node addr:", n.Address(), "weight:", n.InitialWeight())
	}
}

// 测试轮询
func TestPollingBanlance(t *testing.T) {
	rr := NewRoundRobin()
	service := &registry.ServiceInstance{
		ID:      "123aaabbbccddd",
		Name:    "agile-nacos-test-service",
		Version: "1.0",
		Weight:  3,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"http://127.0.0.1:8080?isSecure=false"},
	}
	service2 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		Weight:  2,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8086?isSecure=false"},
	}
	service3 := &registry.ServiceInstance{
		ID:      "aabbcceeff123",
		Name:    "test2",
		Version: "1.0",
		Weight:  1,
		//Metadata:  map[string]string{"idc": "shanghai-xs"},
		Endpoints: []string{"grpc://127.0.0.1:8086?isSecure=false"},
	}
	n1 := NewNode("127.0.0.1:1111", service)
	n2 := NewNode("127.0.0.2:2222", service2)
	n3 := NewNode("127.0.0.2:2223", service3)
	rr.Add(n1, n2, n3)
	for i := 0; i < 30; i++ {
		n, err := rr.Next(context.Background())
		if err != nil {
			t.Log(err)
		}
		t.Log("##node addr:", n.Address(), "weight:", n.InitialWeight())
	}
}
