package config

import (
	"testing"
)

func TestConfig(t *testing.T) {
	c := NewConfig(
		//设置本地配置文件扫描路径
		WithConfigPath("D:\\developer\\workspace\\agileCloud\\gateway\\config"),
		//扫描那种类型的后缀名文件
		WithAllowedExtensions(".xml", ".json", ".yml"))
	c.V.SetDefault("ServiceName", "agile")
	loadErr := c.Load()
	if loadErr != nil {
		t.Error("loadErr:", loadErr)
		return
	}
	b, err := c.Value("dev.enable").Bool()
	if err != nil {
		t.Error("err1:", err)
		return
	}
	appName, err2 := c.Value("agile.application.name").String()
	if err2 != nil {
		t.Error("err2:", err2)
		return
	}
	t.Log("dev.enable:", b, "AppName:", appName)
}

func TestConfig2(t *testing.T) {
	c := NewConfig(
		//设置本地配置文件扫描路径
		WithConfigPath("D:\\developer\\workspace\\agileCloud\\agile-layout\\configs"),
		//扫描那种类型的后缀名文件
		WithAllowedExtensions("yml"))
	loadErr := c.Load()
	if loadErr != nil {
		t.Error("loadErr:", loadErr)
		return
	}
	sqldsn, verr := c.Value("data.database.source").String()
	if verr != nil {
		t.Error(verr)
	}
	t.Log("sqldsn:", sqldsn)
}

type LogConf struct {
	MaxSize    int  `mapstructure:"maxsize"`
	MaxBackUps int  `mapstructure:"maxBackUps"`
	MaxAge     int  `mapstructure:"maxAge"`
	Compress   bool `mapstructure:"compress"`
	LocalTime  bool `mapstructure:"localTime"`
	//LogPath    string `mapstructure:"logPath"`
	Level string `mapstructure:"level"`
}

type TopLevelConf struct {
	*LogConf `mapstructure:"log"`
}

//func TestScanConfig2(t *testing.T) {
//	v := viper.New()
//	v.SetConfigFile("E:\\develop\\workspace\\agileCloud\\agile-layout\\resources\\configs\\config.yaml")
//	err := v.ReadInConfig()
//	if err != nil {
//		fmt.Printf("Error reading config file: %s\n", err)
//		return
//	}
//	var l LogConf
//	err = viper.Unmarshal(&l)
//	if err != nil {
//		fmt.Printf("Error unmarshaling config: %s\n", err)
//		return
//	}
//	t.Log("level", l.Level, "maxsize", l.MaxSize, "maxage", l.MaxAge, "maxbackups", l.MaxBackUps)
//}

func TestScanConfig(t *testing.T) {
	c := NewConfig(
		//设置本地配置文件扫描路径
		WithConfigPath("E:\\develop\\workspace\\agileCloud\\agile-layout\\resources\\configs"),
		//扫描那种类型的后缀名文件
		WithAllowedExtensions("yml"))
	loadErr := c.Load()
	if loadErr != nil {
		t.Error("loadErr:", loadErr)
		return
	}
	var l TopLevelConf
	err := c.Scan(&l)
	if err != nil {
		t.Error("################scan异常:", err)
	}
	t.Log("level", l.Level, "maxsize", l.MaxSize, "maxage", l.MaxAge, "maxbackups", l.MaxBackUps)
	max, verr := c.Value("log.maxsize").Int()
	if verr != nil {
		t.Error(verr)
	}
	t.Log("value.maxsize", max)

}
