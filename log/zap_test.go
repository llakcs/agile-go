package log

import (
	"testing"
)

func TestZapLog(t *testing.T) {
	t.Log("start default log Test!")
	log := NewLogger(NewZapLogger())
	log.Debug("test default log----------------------------------!")
	//log.Debugw("test debugw-----!")
	log.Debugf("test %s", "debug")
	log.Debugw("log", "test debug")
	log.Warn("test warn---------!")
	log.Infof("test %s", "info")
	log.Warn("test warn")
	log.Error("test error")
	log.Errorf("test %s", "error")
	log.Errorw("log", "test error")
	//log.Warnf("test %s", "warn")
}

func TestLog(t *testing.T) {
	//log := With(NewZapLogger(), "serverName:", "test", "ts:", Timestamp(time.RFC3339))
	//log.Log(LevelDebug, "test", "test default log----------------------------------!")
	//log.Log(LevelInfo, "test %s", "debug")
	zlog := NewZapLogger(WithLevel(LevelInfo))
	l := NewLogger(NewFilter(zlog,
		FilterLevel(LevelInfo),
		FilterKey("username"),
		FilterValue("hello")), WithMessageKey("TMsg"))
	l.Log(LevelDebug, "msg", "test debug")
	l.Info("hello")
	l.Infow("password", "123456")
	l.Warnw("username", "kratos")
	l.Error("test error")
	//l.Fatal("test fatal!!")
}
