package dir

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

/*
在工作目录下创建子目录
*/
func CreateWorkingDir(dir string) error {
	// 获取当前工作目录
	workingDir, err := os.Getwd()
	if err != nil {
		fmt.Println("Failed to get current working directory:", err)
		panic("Failed to get current working directory:" + err.Error())
		return nil
	}
	dir = filepath.Join(workingDir, dir)
	mkErr := os.MkdirAll(dir, 0755) // 创建目录时使用权限模式0755
	if errors.Is(mkErr, os.ErrExist) {
		fmt.Println("Directory already exists:", dir)
	} else if mkErr != nil {
		fmt.Println("Failed to create  directory:", mkErr)
	} else {
		fmt.Println("Directory created:", dir)
	}
	return mkErr
}

/*
返回默认系统日志路径
*/
func GetDefaultLogPath() (string, error) {
	workingDir, err := os.Getwd()
	if err != nil {
		fmt.Println("Failed to get current working directory:", err)
		panic("Failed to get current working directory:" + err.Error())
		return "", nil
	}
	dir := filepath.Join(workingDir, "logs")
	return dir, nil
}
